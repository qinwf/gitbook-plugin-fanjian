var path = require('path');
var fs = require('fs');


module.exports = {

    website: {
        assets: "./assets",
        css: [
            "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css"
        ],
        js: [
            "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js",
            "fanjian.js",
            "button.js"
        ]
    }
};
